/**
 * Created on 03.11.18.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */

const {
    get,
    post,
    put,
    expect,
} = require('chakram');
const chakram = require('chakram');
const { describe, it } = require('mocha');
const {
    getApiUrl,
    checkIfPost,
    checkIfResponse,
    checkIfUser,
} = require('../../utils');
const { headerName } = require('../constants');
const {
    list,
    add,
    destroy,
    edit,
    get: getPost,
} = require('./endpoints');
const { add: addUser, destroy: destroyUser } = require('../users/endpoints');

const withLogs = false;
const log = withLogs ? console.log : () => {
};

const getDefaultOptions = token => ({
    headers: {
        [headerName]: token,
    },
});

describe('Posts', () => {
    let initialPostsLength = 0;
    const addedUser = {
        name: 'Posts Testuser',
        email: 'ptu@local.wtl.de',
    };
    const addedPost = {
        message: 'Das ist ein Test!',
    };
    let addedUserToken = '';
    let addedUserId = 0;
    let addedPostId = 0;

    describe('Posts: list', () => {
        it('Should show all existing posts', async () => {
            const response = await get(getApiUrl(list));

            checkIfResponse(response).isSuccessfulJSONResponse();
            checkIfResponse(response).hasCollectionWithName('posts');

            initialPostsLength = response.body.posts.length;
            log('Initial length of posts: ', initialPostsLength);

            if (initialPostsLength > 0) {
                checkIfPost(response.body.posts[0]).andIncludesComments();
                checkIfPost(response.body.posts[0]).andIncludesAuthor();
            }
        });
    });

    describe('Posts: add', () => {
        it('Should add a user', async () => {
            const response = await post(getApiUrl(addUser), addedUser);

            checkIfResponse(response).isSuccessfulJSONResponse();
            checkIfUser(response.body).hasToken();

            expect(response.body.name).to.equal(addedUser.name);
            expect(response.body.email).to.equal(addedUser.email);

            addedUserId = response.body.id;
            addedUserToken = response.body.token;

            log('Added user with id: ', addedUserId);
        });

        it('Should not add a post if no message is provided', async () => {
            const response = await post(getApiUrl(add), {}, getDefaultOptions(addedUserToken));

            checkIfResponse(response).isNotProcessableResponse();
        });

        it('Should add a post', async () => {
            const response = await post(getApiUrl(add), addedPost, getDefaultOptions(addedUserToken));

            checkIfResponse(response).isSuccessfulJSONResponse();
            checkIfPost(response.body).andIncludesAuthor();

            expect(response.body.message).to.equal(addedPost.message);

            addedPostId = response.body.id;

            log('Added post with id: ', addedUserId);
        });

        it('Should not add a post if the user token is wrong', async () => {
            const response = await post(getApiUrl(add), addedPost, getDefaultOptions(`invalid_${addedUserToken}`));

            checkIfResponse(response).isNotSuccessful();
        });

        it('Should not add a post if no token is provided', async () => {
            const response = await post(getApiUrl(add), addedPost);

            checkIfResponse(response).isNotSuccessful();
        });

        it('Should show all existing posts including the added post', async () => {
            const response = await get(getApiUrl(list));

            checkIfResponse(response).isSuccessfulJSONResponse();
            checkIfResponse(response).hasCollectionWithName('posts');

            expect(initialPostsLength + 1).to.equal(response.body.posts.length);

            checkIfPost(response.body.posts[response.body.posts.length - 1]).andIncludesAuthor();
            checkIfPost(response.body.posts[response.body.posts.length - 1]).andIncludesComments();
        });
    });

    describe('Posts: get', () => {
        it('Should get the added post', async () => {
            const response = await get(getApiUrl(getPost, { id: addedPostId }));

            checkIfResponse(response).isSuccessfulJSONResponse();
            checkIfPost(response.body).andIncludesAuthor();
            checkIfPost(response.body).andIncludesComments();

            expect(response.body.message).to.equal(addedPost.message);
            expect(response.body.author.name).to.equal(addedUser.name);
            expect(response.body.author.email).to.equal(addedUser.email);
            expect(response.body.author.id).to.equal(addedUserId);
        });

        it('Should return 404 when the post is not found', async () => {
            const response = await get(getApiUrl(getPost, { id: 0 }));

            checkIfResponse(response).isNotFoundResponse();
        });
    });

    describe('Posts: edit', () => {
        it('Should not edit the added post if no message provided', async () => {
            const response = await put(
                getApiUrl(edit, { id: addedPostId }),
                {},
                getDefaultOptions(addedUserToken),
            );

            checkIfResponse(response).isNotProcessableResponse();
        });

        it('Should edit the added post', async () => {
            addedPost.message = 'Das ist ein zweiter Test.';
            const response = await put(
                getApiUrl(edit, { id: addedPostId }),
                addedPost,
                getDefaultOptions(addedUserToken),
            );

            checkIfResponse(response).isSuccessfulJSONResponse();
            checkIfPost(response.body);

            expect(response.body.message).to.equal(addedPost.message);
        });

        it('Should not edit the added post if the user token is invalid', async () => {
            const response = await put(
                getApiUrl(edit, { id: addedPostId }),
                { ...addedPost, message: 'invalid' },
                getDefaultOptions(`invalid_${addedUserToken}`),
            );

            checkIfResponse(response).isNotSuccessful();
        });

        it('Should not edit the added post if no token is provided', async () => {
            const response = await put(
                getApiUrl(edit, { id: addedPostId }),
                { ...addedPost, message: 'invalid2' },
            );

            checkIfResponse(response).isNotSuccessful();
        });

        it('Should get the edited post', async () => {
            const response = await get(getApiUrl(getPost, { id: addedPostId }));

            checkIfResponse(response).isSuccessfulJSONResponse();
            checkIfPost(response.body).andIncludesAuthor();
            checkIfPost(response.body).andIncludesComments();

            expect(response.body.message).to.equal(addedPost.message);
            expect(response.body.author.name).to.equal(addedUser.name);
            expect(response.body.author.email).to.equal(addedUser.email);
            expect(response.body.author.id).to.equal(addedUserId);
        });
    });

    describe('Posts: delete', () => {
        it('Should not delete the post if no token is provided', async () => {
            const response = await chakram.delete(
                getApiUrl(destroy, { id: addedPostId }),
                {},
            );

            checkIfResponse(response).isNotSuccessful();
        });

        it('Should not delete the post if the provided token is invalid', async () => {
            const response = await chakram.delete(
                getApiUrl(destroy, { id: addedPostId }),
                {},
                getDefaultOptions(`invalid_${addedUserToken}`),
            );

            checkIfResponse(response).isNotSuccessful();
        });

        it('Should still get the post', async () => {
            const response = await get(getApiUrl(getPost, { id: addedPostId }));

            checkIfResponse(response).isSuccessfulJSONResponse();
            checkIfPost(response.body).andIncludesAuthor();
            checkIfPost(response.body).andIncludesComments();

            expect(response.body.message).to.equal(addedPost.message);
            expect(response.body.author.name).to.equal(addedUser.name);
            expect(response.body.author.email).to.equal(addedUser.email);
            expect(response.body.author.id).to.equal(addedUserId);
        });

        it('Should delete the post', async () => {
            const response = await chakram.delete(
                getApiUrl(destroy, { id: addedPostId }),
                {},
                getDefaultOptions(addedUserToken),
            );

            checkIfResponse(response).isSuccessfulJSONResponse();

            log('deleted post with id: ', addedPostId);
        });

        it('Should not find the deleted post', async () => {
            const response = await get(getApiUrl(getPost, { id: addedPostId }));

            checkIfResponse(response).isNotFoundResponse();
        });

        it('Should delete the user', async () => {
            const response = await chakram.delete(getApiUrl(destroyUser, { id: addedUserId }));

            checkIfResponse(response).isSuccessfulJSONResponse();

            log('deleted user with id: ', addedUserId);
        });
    });
});
