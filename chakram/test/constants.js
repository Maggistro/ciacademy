/**
 * Created on 31.10.18.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */

module.exports = {
    baseUrl: 'http://api:80/api',
    headerName: 'token',
};
