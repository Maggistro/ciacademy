/**
 * Created on 31.10.18.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */

const { expect } = require('chakram');

/**
 * Tests if the passed object is a user
 *
 * @param {object} object
 * @return {object}
 * @throws
 */
function checkIfUser(object) {
    expect(object).to.be.an('object');
    expect(object).to.have.property('id');
    expect(object).to.have.property('name');
    expect(object).to.have.property('email');
    expect(object.id).to.be.at.least(1);
    expect(object.name).to.be.a('string');
    expect(object.email).to.be.a('string');

    return {
        ...expect(object),
        hasToken() {
            expect(object).to.have.property('token');
            expect(object.token).to.be.a('string');

            return expect(object);
        },
    };
}

module.exports = checkIfUser;
