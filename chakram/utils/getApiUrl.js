/**
 * Created on 31.10.18.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */

const { replacePlaceholder } = require('bmax-utils');
const { baseUrl } = require('../test/constants');

/**
 * Returns the ful api url
 *
 * @param {string} endpoint
 * @param {object} params
 * @return {string} : The url
 */
function getApiUrl(endpoint, params = {}) {
    return `${baseUrl}${replacePlaceholder(endpoint, params)}`;
}

module.exports = getApiUrl;
